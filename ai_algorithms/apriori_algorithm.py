import random
import pandas as pd
from apyori import apriori

FILE_PATH = './data/search_queries.csv'
MIN_SUPPORT = 0.0001
MIN_CONFIDENCE = 0.2
MIN_LIFT = 1.5
MIN_NUM_OF_RECORDS = 2
MAX_NUM_OF_RECORDS = 2
CSV_OFFSET = 1000

normalize_salary_mapper = {
    'lt30': 0,
    'lt50': 30,
    'lt90': 50,
    'gt90': 90,
}

normalize_experience_mapper = {
    'lt2': 0,
    'lt5': 2,
    'lt7': 5,
    'lt10': 7,
    'gt10': 10,
}


def get_criterion_original_data_from_normalized_criterion(normalized_criterion=''):
    original_name_value = normalized_criterion.split('_')
    original_name = original_name_value[0]
    original_value = original_name_value[1]
    return original_name, original_value


def normalize_enum(name, value):
    if value is None:
        return None
    return name + '_' + value


def get_original_enum(normalized_value):
    return normalized_value


def get_original_salary(normalized_value):
    if normalized_value in normalize_salary_mapper:
        return normalize_salary_mapper[normalized_value]
    else:
        return None


def get_original_experience(normalized_value):
    if normalized_value in normalize_experience_mapper:
        return normalize_experience_mapper[normalized_value]
    else:
        return None


def normalize_salary(name, value):
    normal_salary = name + '_'
    if value < 30:
        normal_salary += 'lt30'
    elif value < 50:
        normal_salary += 'lt50'
    elif value < 90:
        normal_salary += 'lt90'
    else:
        normal_salary += 'gt90'
    return normal_salary


def normalize_experience(name, value):
    normal_salary = name + '_'
    if value < 2:
        normal_salary += 'lt2'
    elif value < 5:
        normal_salary += 'lt5'
    elif value < 7:
        normal_salary += 'lt7'
    elif value < 10:
        normal_salary += 'lt10'
    else:
        normal_salary += 'gt10'
    return normal_salary


normalize_method_mapper = {
    'areaStr': normalize_enum,
    'degreeStr': normalize_enum,
    'occupationStr': normalize_enum,
    'requestedSalary': normalize_salary,
    'yearsOfExperience': normalize_experience,
}

normalize_get_original_value_mapper = {
    'areaStr': get_original_enum,
    'degreeStr': get_original_enum,
    'occupationStr': get_original_enum,
    'requestedSalary': get_original_salary,
    'yearsOfExperience': get_original_experience,
}


def get_original_value(original_name, normalized_value):
    if original_name in normalize_get_original_value_mapper:
        return normalize_get_original_value_mapper[original_name](normalized_value)
    else:
        return None


def get_best_lift(association_results, normalized_criterion, used_lifts_dict):
    lift_criterion = None

    for rule in association_results:
        left, right = rule[0]
        if left != normalized_criterion:
            continue
        if right in used_lifts_dict:
            continue
        lift_criterion = right
        break

    if lift_criterion is None:
        return None

    used_lifts_dict[lift_criterion] = True
    original_name, normalized_value = get_criterion_original_data_from_normalized_criterion(lift_criterion)
    original_value = get_original_value(original_name, normalized_value)
    return {
        'name': original_name,
        'value': original_value
    }


def get_normalized_criterion(criterion_name, criterion_value):
    if criterion_value is None:
        return None
    if criterion_name in normalize_method_mapper:
        return normalize_method_mapper[criterion_name](criterion_name, criterion_value)


def get_hash_key_for_query(query):
    return hash(frozenset(query.items()))


def is_query_ok(query):
    if None in query:
        return False
    return True


def get_distinct_queries(queries):
    distinct_dict = {}
    for query in queries:
        if not is_query_ok(query):
            continue
        key = get_hash_key_for_query(query)
        distinct_dict[key] = query

    return distinct_dict.values()


def get_ai_search_query(criteria={}):
    used_lifts_dict = {}
    needed_lifts = {}
    queries = []

    if len(criteria) < 1:
        return None

    print criteria

    for key, value in criteria.iteritems():
        normalized_criterion = get_normalized_criterion(key, value)
        if normalized_criterion is None:
            continue
        needed_lifts[normalized_criterion] = value

    if len(needed_lifts) < 1:
        return None

    store_data = pd.read_csv(FILE_PATH,header=None,skip_blank_lines=True)
    if len(needed_lifts) > 1:
        df_search_criteria = pd.DataFrame([needed_lifts.keys()])
        df_search_criteria.to_csv(FILE_PATH, mode='a+', index=False, header=False, line_terminator='\n')

    rows, cols = store_data.shape
    if rows == 0:
        return None
    records = []
    start_index_by_offset = rows - CSV_OFFSET if rows >= CSV_OFFSET else 0
    for i in xrange(start_index_by_offset, rows):
        records.append([str(store_data.values[i, j]) for j in range(0, cols)])

    association_rules = apriori(records,
                                min_support=MIN_SUPPORT,
                                min_confidence=MIN_CONFIDENCE,
                                min_lift=MIN_LIFT,
                                min_length=MIN_NUM_OF_RECORDS,
                                max_length=MAX_NUM_OF_RECORDS)
    association_results = list(association_rules)
    association_rules_sorted = sorted(association_results, key=lambda rule: rule[2][0][3], reverse=True)
    criterion_query_1 = {}
    criterion_query_2 = {}

    for criterion, original_value in needed_lifts.iteritems():
        original_name, _ = get_criterion_original_data_from_normalized_criterion(criterion)
        criterion_query_1[original_name] = original_value
        criterion_query_2[original_name] = original_value

    for criterion, original_value in needed_lifts.iteritems():
        original_name, _ = get_criterion_original_data_from_normalized_criterion(criterion)
        right_criterion = get_best_lift(association_rules_sorted, criterion, used_lifts_dict)
        if right_criterion is not None:
            if right_criterion['name'] in criterion_query_1:
                if right_criterion['value'] == criterion_query_1[right_criterion['name']]:
                    continue
                criterion_query_1_temp = dict(criterion_query_1)
                criterion_query_1_temp[right_criterion['name']] = right_criterion['value']
                queries.append(criterion_query_1_temp)
            else:
                criterion_query_2_temp = dict(criterion_query_2)
                criterion_query_2_temp[original_name] = None
                to_be_replaced_candidates = [x for x in criterion_query_2_temp
                                             if
                                             criterion_query_2_temp[x] is not None]
                if len(to_be_replaced_candidates) < 1:
                    continue
                rnd_criterion = random.choice(to_be_replaced_candidates)
                criterion_query_2_temp[rnd_criterion] = None
                criterion_query_2_temp[right_criterion['name']] = right_criterion['value']
                criterion_query_2_temp[original_name] = original_value
                queries.append(criterion_query_2_temp)

    queries = get_distinct_queries(queries)
    return queries

