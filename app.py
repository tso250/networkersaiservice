from flask import Flask, jsonify
from flask.json import request

from ai_algorithms.apriori_algorithm import get_ai_search_query

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/search', methods=['POST'])
def search():
    search_query = request.get_json()
    ai_query = get_ai_search_query(search_query)
    result = jsonify(ai_query)
    return result


if __name__ == '__main__':
    app.run()
